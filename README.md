# e3-crome

ESS Site-specific EPICS module : crome

This is the e3 wrapper for the device support module for the Crome REMS monitor devices.

## Dependencies
This module depends on:

- *e3-base* version 7.0.5 or later
- *e3-require* version 3.4.1 or later
- *e3-asyn* 4.41 or later

Without these dependencies the module will not compile or run

## Known issues
Starting an IOC based on this module may cause a crash in two known scenarios:
1. If the host name given for the REMS monitor does not resolve the IOC shell will crash and genearte a core dump. Be careful as this will quicklt fill up your hard drive
2. If the host name resolves (or a static IP was given) but the REMS monitor cannot be reached the IOC shell will also crash., but without generating a core dump
