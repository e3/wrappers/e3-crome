#
#  Copyright (c) 2019 - 2020, European Spallation Source ERIC
#
#  The program is free software: you can redistribute it and/or modify it
#  under the terms of the BSD 3-Clause license.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP:=cromeApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src
APPSUP:=$(APP)/../RomulusSup

TEMPLATES += $(wildcard $(APPDB)/*.db)

USR_INCLUDES += -I$(where_am_I)$(APPSRC)
USR_INCLUDES += -I$(where_am_I)$(APPSUP)

SOURCES   += $(APPSRC)/crome.cpp

DBDS   += $(APPSRC)/cromeSupport.dbd

HEADERS   += $(APPSRC)/crome.h

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)


ifeq (linux-x86_64, $(findstring linux-x86_64,$(T_A)))
USR_LDFLAGS  += -Wl,--enable-new-dtags
USR_LDFLAGS  += -L$(where_am_I)/$(APPSUP)/os/linux-x86_64
USR_LDFLAGS  += -Wl,-rpath,"\$$ORIGIN/../../../../../siteLibs/vendor/$(E3_MODULE_NAME)/$(E3_MODULE_VERSION)"
LIB_SYS_LIBS += ROMULUS
endif

VENDOR_LIBS += $(APPSUP)/os/linux-x86_64/libROMULUS.so
VENDOR_LIBS += $(APPSUP)/os/linux-x86_64/libROMULUS.so.6


.PHONY: vlibs
vlibs: $(VENDOR_LIBS)

.PHONY: $(VENDOR_LIBS)
$(VENDOR_LIBS):
	install -m 755 -d $(E3_MODULES_VENDOR_LIBS_LOCATION)/
	install -m 644 $@ $(E3_MODULES_VENDOR_LIBS_LOCATION)/
